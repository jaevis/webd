﻿Public Class FileItem
    Inherits TreeViewItem

    Private _fileinfo As IO.FileInfo
    Private _filetype As fileType

    Public Sub New(Filelocation As String)
        Me.New(New IO.FileInfo(Filelocation))
    End Sub

    Public Sub New(Fileinfo As IO.FileInfo)
        _fileinfo = Fileinfo

        Select Case _fileinfo.Extension
            Case "html", "htm"
                _filetype = WebD.fileType.HTML
            Case "css"
                _filetype = WebD.fileType.CSS
            Case "js"
                _filetype = WebD.fileType.JavaScript
            Case "PHP", "php"
                _filetype = WebD.fileType.PHP
        End Select
    End Sub

    Public ReadOnly Property Fileinfo As IO.FileInfo
        Get
            Return _fileinfo
        End Get
    End Property

    Public ReadOnly Property FileType As fileType
        Get
            Return _filetype
        End Get
    End Property


End Class

Public Enum fileType
    HTML
    CSS
    JavaScript
    PHP
End Enum
