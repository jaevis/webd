﻿Public Class FolderItem
    Inherits TreeViewItem

    Private _FolderInfo As IO.DirectoryInfo

    Public Sub New(Folderlocation As String)
        Me.New(New IO.DirectoryInfo(Folderlocation))
    End Sub

    Public Sub New(DirectoryInfo As IO.DirectoryInfo)
        _FolderInfo = DirectoryInfo

        ' Load Folders
        Dim childFolder = _FolderInfo.EnumerateDirectories
        For Each folder As IO.DirectoryInfo In childFolder
            Me.AddChild(New FolderItem(folder))
        Next

        ' Load Files
        Dim childFiles = _FolderInfo.EnumerateFiles
        For Each file As IO.FileInfo In childFiles
            Me.AddChild(New FileItem(file))
        Next

    End Sub


    Public ReadOnly Property FolderInfo As IO.DirectoryInfo
        Get
            Return _FolderInfo
        End Get
    End Property

   


End Class
